package br.ucsal.domain;

public enum TipoCargaEnum {
	 LiQUIDOS("liquido",1), 
	 GAS("gas",2), 
	 SOLIDOS("solido",3);
	
	private String descricao;
	private Integer valor;

	

	private TipoCargaEnum(String descricao, Integer valor) {
		this.descricao = descricao;
		this.valor = valor;
	}



	public String getDescricao() {
		return descricao;
	}



	public Integer getValor() {
		return valor;
	}
	
	
	
	
}
