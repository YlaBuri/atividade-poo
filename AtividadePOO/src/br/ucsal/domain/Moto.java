package br.ucsal.domain;

public class Moto extends Veiculo{
	private String nomeModelo;

	
	public Moto(String placa, Integer anoFabricacao, Double valor, String nomeModelo) {
		super(placa, anoFabricacao, valor);
		this.nomeModelo = nomeModelo;
	}

	public String getNomeModelo() {
		return nomeModelo;
	}

	public void setNomeModelo(String nomeModelo) {
		this.nomeModelo = nomeModelo;
	}
	
	
}
