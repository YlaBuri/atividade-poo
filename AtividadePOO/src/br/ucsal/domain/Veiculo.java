package br.ucsal.domain;

public abstract class Veiculo implements Comparable<Veiculo>{
	private String placa;
	private Integer anoFabricacao;
	private Double valor;
	
	public Veiculo(String placa, Integer anoFabricacao, Double valor) {
		super();
		this.placa = placa;
		this.anoFabricacao = anoFabricacao;
		this.valor = valor;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public Integer getAnoFabricacao() {
		return anoFabricacao;
	}

	public void setAnoFabricacao(Integer anoFabricacao) {
		this.anoFabricacao = anoFabricacao;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	@Override
	public int compareTo(Veiculo o) {
		return placa.compareTo(o.getPlaca());
	}

	@Override
	public String toString() {
		return "Veiculo [placa=" + placa + ", anoFabricacao=" + anoFabricacao + ", valor=" + valor + "]";
	}
	
	
	
}
