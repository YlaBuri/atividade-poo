package br.ucsal.domain;

public class Carro extends Veiculo{
	private Integer qtfPortas;

	public Carro(String placa, Integer anoFabricacao, Double valor, Integer qtfPortas) {
		super(placa, anoFabricacao, valor);
		this.qtfPortas = qtfPortas;
	}

	public Integer getQtfPortas() {
		return qtfPortas;
	}

	public void setQtfPortas(Integer qtfPortas) {
		this.qtfPortas = qtfPortas;
	}
	
	
}
