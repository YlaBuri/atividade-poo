package br.ucsal.domain;

public class Caminhao extends Veiculo{
	private Integer qtdEixos;
	private TipoCargaEnum tipoCarga;	

	public Caminhao(String placa, Integer anoFabricacao, Double valor, Integer qtdEixos, TipoCargaEnum tipoCarga) {
		super(placa, anoFabricacao, valor);
		this.qtdEixos = qtdEixos;
		this.tipoCarga = tipoCarga;
	}

	public Integer getQtdEixos() {
		return qtdEixos;
	}

	public void setQtdEixos(Integer qtdEixos) {
		this.qtdEixos = qtdEixos;
	}

	public TipoCargaEnum getTipoCarga() {
		return tipoCarga;
	}

	public void setTipoCarga(TipoCargaEnum tipoCarga) {
		this.tipoCarga = tipoCarga;
	}
	
	
}
