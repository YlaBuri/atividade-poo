package br.ucsal.persistence;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import br.ucsal.business.VeiculoBO;
import br.ucsal.domain.Caminhao;
import br.ucsal.domain.Carro;
import br.ucsal.domain.Moto;
import br.ucsal.domain.TipoCargaEnum;
import br.ucsal.domain.Veiculo;

public class VeiculoDAO {
	private static List<Veiculo> veiculos = new ArrayList<Veiculo>();


	public static void incluirCarro(String placa, Integer anoFabricacao, Double valor, Integer qtfPortas) throws Exception {
		Carro c = new Carro(placa, anoFabricacao, valor, qtfPortas);
		veiculos.add(VeiculoBO.validarVeiculo(c));
	}

	public static void incluirCaminhao(String placa, Integer anoFabricacao, Double valor, Integer qtdEixos, TipoCargaEnum tipoCarga) throws Exception {

		Caminhao ca = new Caminhao(placa, anoFabricacao, valor, qtdEixos, tipoCarga);
		ca= VeiculoBO.validarCarga(ca);
		veiculos.add(VeiculoBO.validarVeiculo(ca));
	}

	public static void incluirMoto(String placa, Integer anoFabricacao, Double valor, String nomeModelo) throws Exception {
		Moto m = new Moto(placa, anoFabricacao, valor, nomeModelo);
		veiculos.add(VeiculoBO.validarVeiculo(m));
	}

	public static void ordenarPorPlaca() {
		Collections.sort(veiculos);
		for (Veiculo veiculo : veiculos) {
			System.out.println(veiculo);
		}
	}

	public static void ordenarPorValor() {
		Collections.sort(veiculos, new Comparator<Veiculo>() {
			@Override
			public int compare(Veiculo v1, Veiculo v2) {
				return v1.getValor().compareTo(v2.getValor());
			}			
		});

		for (Veiculo veiculo : veiculos) {
			System.out.println(veiculo);
		}
	}

	//Listar os diferentes nomes de modelo de motos cadastradas no sistemas; 
	public static void listarModelos() {
		Set<String> modelos = new HashSet<String>();
		for (Veiculo veiculo : veiculos) {
			if(veiculo instanceof Moto) {
				modelos.add( ((Moto) veiculo).getNomeModelo() );
			}
		}
		for (String modelo : modelos) {
			System.out.println(modelo);
		}
	}

	//Listar a quantidade de ve�culos por ano de fabrica��o. 
	public static void listarPorAnoFabricacao() {
		Map<Integer, Integer> veiculoAno =new HashMap<Integer, Integer>();

		for (Veiculo veiculo : veiculos) {
			if(!veiculoAno.containsKey(veiculo.getAnoFabricacao())) {
				veiculoAno.put(veiculo.getAnoFabricacao(), 1);
			}else {
				int qtd = veiculoAno.get(veiculo.getAnoFabricacao());
				veiculoAno.put(veiculo.getAnoFabricacao(), qtd+1);
			}
		}

		for (Integer chave : veiculoAno.keySet()) {
			System.out.println(chave+"="+ veiculoAno.get(chave));
		}
	}

	public static void ordenarAnoValor() {
		Collections.sort(veiculos, new Comparator<Veiculo>() {

			@Override
			public int compare(Veiculo o1, Veiculo o2) {
				Integer comparacao = o1.getAnoFabricacao().compareTo(o2.getAnoFabricacao());
				if(comparacao==0) {
					comparacao= o1.getValor().compareTo(o2.getValor());
				}
				return comparacao;
			}
			
		});
		
		for (Veiculo veiculo : veiculos) {
			System.out.println(veiculo);
		}
	}

		
	
}
