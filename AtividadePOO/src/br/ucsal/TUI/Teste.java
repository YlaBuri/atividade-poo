package br.ucsal.TUI;

import br.ucsal.domain.TipoCargaEnum;
import br.ucsal.persistence.VeiculoDAO;

public class Teste {

	public static void main(String[] args) {
		
		try {
			VeiculoDAO.incluirCaminhao("fff", 2000, 1000.90, 5, TipoCargaEnum.LiQUIDOS);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		try {
			VeiculoDAO.incluirCarro("bbb", 2010, 233.90, 4);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	
		try {
			VeiculoDAO.incluirMoto("www", 2000, 1900.45, "modelo2");
			VeiculoDAO.incluirMoto("ccc", 2019, 5450.10, "modelo1");
			VeiculoDAO.incluirMoto("ddd", 2019, 12900.23, "modelo1");
			VeiculoDAO.incluirMoto("aaa", 2000, 100.12, "modelo2");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("---ordenarPorValor()");
		VeiculoDAO.ordenarPorValor();
		System.out.println("---ordenarPorPlaca()");
		VeiculoDAO.ordenarPorPlaca();
		System.out.println("---listarModelos()");
		VeiculoDAO.listarModelos();
		System.out.println("---listarPorAnoFabricacao()");
		VeiculoDAO.listarPorAnoFabricacao();
		System.out.println("---ordenarAnoValor()");
		VeiculoDAO.ordenarAnoValor();;
	}

}
