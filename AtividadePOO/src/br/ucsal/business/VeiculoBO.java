package br.ucsal.business;

import br.ucsal.domain.Caminhao;
import br.ucsal.domain.Carro;
import br.ucsal.domain.Moto;
import br.ucsal.domain.TipoCargaEnum;
import br.ucsal.domain.Veiculo;

public class VeiculoBO {
	/*
	 * Quantidade de portas, de eixos, ano de fabrica��o e valor devem ser maiores
		que zero;
 		Ve�culo cujo tipo de carga seja l�quidos devem ter, no m�nimo 4 eixos;
	 	O nome do modelo (para motos) e a placa (para todos os ve�culos) s�o
		obrigat�rios. 
	 */
	
	public static Veiculo validarVeiculo(Veiculo v) throws Exception {
		if(v.getPlaca()!= null && v.getAnoFabricacao()>0 && v.getValor()>0.0) {
			if(v instanceof Carro) {
				if(((Carro) v).getQtfPortas()>0) {
					return v;
				}else {
					throw new Exception("Numero de portas n�o pode ser menor q 0");
				}
			}
			if(v instanceof Caminhao) {
				if(((Caminhao) v).getQtdEixos()>0) {
					return v;
				}else {
					throw new Exception("Numero de eixos n�o pode ser menor q 0");
				}
			}
			if(v instanceof Moto) {
				if(((Moto) v).getNomeModelo()!= null) {
					return v;
				}else {
					throw new Exception("Nome do modelo n�o pode ser nulo");
				}
			}
		}else {
			throw new Exception("Valor n�o pode ser nulo");
		}
		return v;
		
	}
	
	public static Caminhao validarCarga(Caminhao c) throws Exception{
		if(c.getTipoCarga()==TipoCargaEnum.LiQUIDOS && c.getQtdEixos()<4) {
			throw new Exception("Quantidade de eixos inv�lido par aeste tipo de carga");
		}else {
			return c;
		}
		
	}
	
	
}
